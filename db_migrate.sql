USE test_gitlab_ci;

DROP TABLE IF EXISTS
    customer;
	
CREATE TABLE customer(
    customer_name VARCHAR(255) DEFAULT NULL
);	

INSERT INTO customer(customer_name)
VALUES('FEDOR'),('ALINA'),('ROMA');

DROP TABLE IF EXISTS
    account;
	
CREATE TABLE account(
    account_no VARCHAR(255) DEFAULT NULL
);	

INSERT INTO account(account_no)
VALUES('0001'),('1000'),('2019');
	
DROP TABLE IF EXISTS
    kubedb_table;

CREATE TABLE kubedb_table(
    id BIGINT(20) NOT NULL,
    name VARCHAR(255) DEFAULT NULL
);

--
-- Dumping data for table kubedb_table
--

INSERT INTO kubedb_table(id, name)
VALUES(1, 'name1'),(2, 'name2'),(3, 'name3');

--
-- Indexes for table kubedb_table
--

ALTER TABLE
    kubedb_table ADD PRIMARY KEY(id);

--
-- AUTO_INCREMENT for table kubedb_table
--

ALTER TABLE
    kubedb_table MODIFY id BIGINT(20) NOT NULL AUTO_INCREMENT,
AUTO_INCREMENT = 4;

DROP TABLE IF EXISTS
    deal;

	CREATE TABLE deal(
    deal_number VARCHAR(255) DEFAULT NULL
);

INSERT INTO deal(deal_number)
VALUES('TTT345'),('MMM239'),('RRR748');

DROP TABLE IF EXISTS
    employee;

	CREATE TABLE employee(
    pernr VARCHAR(255) DEFAULT NULL
);

INSERT INTO employee(pernr)
VALUES('00001'),('00002'),('00003');


COMMIT;