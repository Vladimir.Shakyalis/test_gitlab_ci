var http = require('http');
var handleRequest = function(request, response) {
  response.writeHead(200);
  response.end('Hello World! :) 2019-06-10 And Hello World Again :-D');
};
var helloServer = http.createServer(handleRequest);
helloServer.listen(8080);